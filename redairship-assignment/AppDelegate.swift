//
//  AppDelegate.swift
//  redairship-assignment
//
//  Created by Cao Thang Hung on 04/10/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        configureInitialViewController()
        
        setupNavigationBarAppearance()
        
        return true
    }
    
    private func configureInitialViewController() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let mainViewController = MainViewController()
        window.rootViewController = UINavigationController(rootViewController: mainViewController)
        window.makeKeyAndVisible()
        self.window = window
    }
    
    private func setupNavigationBarAppearance() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .white
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = UINavigationBar.appearance().standardAppearance
    }
}

