//
//  PaymentViewController.swift
//  redairship-assignment
//
//  Created by Cao Thang Hung on 20/10/2021.
//

import UIKit

class PaymentViewController: MainChildViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        scrollView = UIScrollView().apply {
            self.view.addSubview($0)
            $0.clipsToBounds = false
            $0.fillConstraint(to: self.view)
        }

        let contentView = UIView().apply {
            scrollView.addSubview($0)
            $0.fillConstraint(to: scrollView)

            NSLayoutConstraint.activate([
                $0.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            ])
        }
        
        let balanceInfoView = UIView().apply {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
            
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: contentView.topAnchor),
                $0.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -(CardViewController.OVERLAP_CARD_WIDTH)),
                $0.heightAnchor.constraint(equalToConstant: CardViewController.CARD_HEIGHT + 32)
            ])
        }
        
        let balanceLabel = UILabel().apply {
            $0.text = "Balance: "
            $0.translatesAutoresizingMaskIntoConstraints = false
            balanceInfoView.addSubview($0)
            
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: balanceInfoView.topAnchor, constant: 64),
                $0.leadingAnchor.constraint(equalTo: balanceInfoView.leadingAnchor, constant: 16),
                $0.trailingAnchor.constraint(equalTo: balanceInfoView.trailingAnchor, constant: -(CardViewController.OVERLAP_CARD_WIDTH + 16)),
            ])
        }
        
        let amountLabel = UILabel().apply {
            $0.text = "1,000,000 $"
            $0.font = .boldSystemFont(ofSize: 32)
            $0.translatesAutoresizingMaskIntoConstraints = false
            balanceInfoView.addSubview($0)
            
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: balanceLabel.bottomAnchor, constant: 16),
                $0.leadingAnchor.constraint(equalTo: balanceInfoView.leadingAnchor, constant: 16),
                $0.trailingAnchor.constraint(equalTo: balanceInfoView.trailingAnchor, constant: -(CardViewController.OVERLAP_CARD_WIDTH + 16)),
            ])
        }
        
        let transferButton = UIButton().apply {
            $0.setTitle("Pay or transfer", for: .normal)
            $0.backgroundColor = .systemBlue
            $0.setTitleColor(.white, for: .normal)
            $0.layer.cornerRadius = 5.0
            $0.clipsToBounds = true
            $0.translatesAutoresizingMaskIntoConstraints = false
            balanceInfoView.addSubview($0)
            
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: balanceInfoView.bottomAnchor),
                $0.leadingAnchor.constraint(equalTo: balanceInfoView.leadingAnchor, constant: 16),
                $0.trailingAnchor.constraint(equalTo: balanceInfoView.trailingAnchor, constant: -16),
                $0.heightAnchor.constraint(equalToConstant: 50)
            ])
        }
        
        let tableViewLabel = UILabel().apply {
            $0.text = "This is Table View 1"
            $0.textAlignment = .center
            $0.layer.cornerRadius = 5.0
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.black.cgColor
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
            
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: balanceInfoView.bottomAnchor, constant: 16),
                $0.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 16),
                $0.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
                $0.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
                $0.heightAnchor.constraint(equalToConstant: 700)
            ])
        }
    }
}
