//
//  MainChildViewController.swift
//  redairship-assignment
//
//  Created by Cao Thang Hung on 24/10/2021.
//

import UIKit

class MainChildViewController: UIViewController, MainPageScrollDelegate {
    
    var scrollView: UIScrollView!
    
    override func didMove(toParent parent: UIViewController?) {
        if let parent = parent as? MainViewController {
            parent.scrollDelegates.append(self)
            scrollView.delegate = parent
        }
    }
    
    func didScrollHorizontalWithPercentage(percentage: Double) {}
    
    func didScrollVertical(scrollView: UIScrollView) {
        if scrollView != self.scrollView {
            self.scrollView.bounds.origin = scrollView.contentOffset
        }
    }
    
    func scrollVerticalToTop() {
        scrollView.setContentOffset(.zero, animated: true)
    }
}
