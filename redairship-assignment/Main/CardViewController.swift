//
//  CardViewController.swift
//  redairship-assignment
//
//  Created by Cao Thang Hung on 20/10/2021.
//

import UIKit

class CardViewController: MainChildViewController {

    static let OVERLAP_CARD_WIDTH: CGFloat = 50
    static let CARD_WIDTH: CGFloat = 200
    static let CARD_HEIGHT: CGFloat = 1.6 * CARD_WIDTH
    
    private var cardImageView: UIImageView!
    private var showCardDetailButton: UIButton!
    private var freezeCardButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        
        scrollView = UIScrollView().apply {
            self.view.addSubview($0)
            $0.clipsToBounds = false
            $0.fillConstraint(to: self.view)
        }

        let contentView = UIView().apply {
            scrollView.addSubview($0)
            $0.fillConstraint(to: scrollView)
            
            NSLayoutConstraint.activate([
                $0.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            ])
        }
        
        cardImageView = UIImageView().apply {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.image = UIImage(named: "credit-card")
            contentView.addSubview($0)
            
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 32),
                $0.widthAnchor.constraint(equalToConstant: CardViewController.CARD_WIDTH),
                $0.heightAnchor.constraint(equalToConstant: CardViewController.CARD_HEIGHT),
                $0.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16)
            ])
            $0.transform = CGAffineTransform(translationX: -(CardViewController.OVERLAP_CARD_WIDTH + 16), y: 0)
        }
        
        showCardDetailButton = UIButton().apply {
            $0.setTitle("Show card\n details", for: .normal)
            $0.setTitleColor(.black, for: .normal)
            $0.titleLabel?.numberOfLines = 0
            $0.titleLabel?.textAlignment = .center
            $0.layer.borderColor = UIColor.black.cgColor
            $0.layer.borderWidth = 1
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        
        freezeCardButton = UIButton().apply {
            $0.setTitle("Freeze card", for: .normal)
            $0.setTitleColor(.black, for: .normal)
            $0.layer.borderColor = UIColor.black.cgColor
            $0.layer.borderWidth = 1
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        
        let _ = UIStackView(arrangedSubviews: [showCardDetailButton, freezeCardButton]).apply {
            $0.axis = .vertical
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.spacing = 32
            contentView.addSubview($0)
            
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(greaterThanOrEqualTo:  cardImageView.topAnchor),
                $0.leadingAnchor.constraint(equalTo: cardImageView.trailingAnchor, constant: 16),
                $0.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
                $0.centerYAnchor.constraint(equalTo: cardImageView.centerYAnchor),
                $0.bottomAnchor.constraint(lessThanOrEqualTo: cardImageView.bottomAnchor)
            ])
        }
        
        let tableViewLabel = UILabel().apply {
            $0.text = "This is Table View 2"
            $0.textAlignment = .center
            $0.layer.cornerRadius = 5.0
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.black.cgColor
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
            
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: cardImageView.bottomAnchor, constant: 16),
                $0.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 16),
                $0.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
                $0.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
                $0.heightAnchor.constraint(equalToConstant: 700)
            ])
        }
    }
    
    override func didScrollHorizontalWithPercentage(percentage: Double) {
        cardImageView.transform = CGAffineTransform(translationX: ((CardViewController.OVERLAP_CARD_WIDTH + 16)*percentage) - (CardViewController.OVERLAP_CARD_WIDTH + 16), y: 0)
    }
}
