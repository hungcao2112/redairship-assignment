//
//  MainViewController.swift
//  redairship-assignment
//
//  Created by Cao Thang Hung on 20/10/2021.
//

import Foundation
import UIKit

protocol MainPageScrollDelegate: AnyObject {
    func didScrollHorizontalWithPercentage(percentage: Double)
    func didScrollVertical(scrollView: UIScrollView)
    func scrollVerticalToTop()
}

class MainViewController: UIViewController {
    
    fileprivate lazy var pages: [UIViewController] = [
        PaymentViewController(),
        CardViewController()
    ]
    
    private var currentIndex: Int = 0
    private var scrollView: UIScrollView = UIScrollView()
    private var collectionView: UICollectionView!
    
    var scrollDelegates: [MainPageScrollDelegate] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Bank Account"
        setupNavigationBar()
        setupView()
    }
    
    private func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.backgroundColor = .white
        
        let leftButton = UIBarButtonItem(image: UIImage(systemName: "chevron.backward"), style: .plain, target: self, action: #selector(leftBarButtonDidTap))
        navigationItem.leftBarButtonItem = leftButton
        
        let rightButton = UIBarButtonItem(image: nil, style: .plain, target: self, action: #selector(rightBarButtonDidTap))
        rightButton.title = "Manage"
        navigationItem.rightBarButtonItem = rightButton
    }
    
    private func setupCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.apply {
            self.view.addSubview($0)
            $0.isPagingEnabled = true
            $0.bounces = false
            $0.register(ChildViewControllerCell.self, forCellWithReuseIdentifier: ChildViewControllerCell.simpleClassName())
            $0.delegate = self
            $0.dataSource = self
            $0.fillConstraint(to: self.view)
        }
    }
    
    private func setupView() {
        view.backgroundColor = .white
        edgesForExtendedLayout = []
        setupCollectionView()
    }
    
    @objc private func leftBarButtonDidTap() {
        collectionView.isPagingEnabled = false
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        collectionView.isPagingEnabled = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.scrollDelegates.forEach {
                $0.scrollVerticalToTop()
            }
        }
    }
    
    @objc private func rightBarButtonDidTap() {
        collectionView.isPagingEnabled = false
        collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .centeredHorizontally, animated: true)
        collectionView.isPagingEnabled = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.scrollDelegates.forEach {
                $0.scrollVerticalToTop()
            }
        }
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChildViewControllerCell.simpleClassName(), for: indexPath) as? ChildViewControllerCell else {
            return UICollectionViewCell()
        }
        cell.addChildViewController(parent: self, child: pages[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return -1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return -1
    }
}

extension MainViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView {
            let percentage = Double(scrollView.contentOffset.x/scrollView.frame.width)
            scrollDelegates.forEach {
                $0.didScrollHorizontalWithPercentage(percentage: percentage)
            }
            
        }
        else {
            scrollDelegates.forEach {
                $0.didScrollVertical(scrollView: scrollView)
            }
        }
    }
}
