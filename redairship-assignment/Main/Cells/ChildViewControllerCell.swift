//
//  ChildViewControllerCell.swift
//  redairship-assignment
//
//  Created by Cao Thang Hung on 22/10/2021.
//

import UIKit

class ChildViewControllerCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addChildViewController(parent: UIViewController, child: UIViewController) {
        self.addSubview(child.view)
        child.view.fillConstraint(to: self)
        child.didMove(toParent: parent)
    }
}
