//
//  NSObjectExtension.swift
//  redairship-assignment
//
//  Created by Cao Thang Hung on 20/10/2021.
//

import Foundation

extension NSObject {
    static func simpleClassName() -> String {
        return NSStringFromClass(self).components(separatedBy: ".").last ?? ""
    }
}
