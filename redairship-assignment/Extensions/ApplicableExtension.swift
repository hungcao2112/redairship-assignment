//
//  ApplicableExtension.swift
//  redairship-assignment
//
//  Created by Cao Thang Hung on 20/10/2021.
//

import Foundation

protocol Applicable {}
extension Applicable {
    @discardableResult
    func apply(block: (Self) -> Void) -> Self {
        block(self)
        return self
    }

    func lets<R>(block: (Self) -> R) -> R {
        return block(self)
    }
}

extension NSObject: Applicable {}
extension Int: Applicable {}
extension Int64: Applicable {}
extension Double: Applicable {}
extension String: Applicable {}
extension Array: Applicable {}
extension Bool: Applicable {}
