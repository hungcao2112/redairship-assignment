//
//  UIViewExtension.swift
//  redairship-assignment
//
//  Created by Cao Thang Hung on 20/10/2021.
//

import Foundation
import UIKit

extension UIView {
    
    func fillConstraint(to view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
}
